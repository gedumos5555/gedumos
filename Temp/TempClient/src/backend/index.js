const { session, app, BrowserWindow } = require('electron');
const path = require('path');

// Referinta globala a ferestrei.
// Necesara. In lipsa acestei referinte, GC-ul ES va inchide automat fereastra.
let win;

function createWindow() {
    // Deschidere fereastra.
    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.loadFile(path.join(__dirname, 'index.html'));

    // Evenimente fereastra.
    // Eveniment inchidere fereastra.
    win.on('closed', () => {
        win = null;
    });
}

// Referinta globala app este furnizata de electron.
createApp();

function createApp() {
    // Evenimente aplicatie.
    // Eveniment pornire aplicatie.
    app.on('ready', () => {
        // Creaza o fereastra aferenta.
        createWindow();
    });

    // Eveniment 'toate ferestrele inchise'.
    app.on('window-all-closed', () => {
        // In MacOS este obisnuit ca aplicatiile sa ramana active chiar
        // daca ferestrele au fost inchise complet.
        // Utilizatorii MacOS inchid aplicatiile complet prin Cmd + Q.
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });

    // Eveniment click iconita aplicatie.
    app.on('activate', () => {
        // In MacOS, daca nu este nici o fereastra activa, in momentul
        // apasarii iconitei aplicatiei, se deschide o fereastra noua.
        if (win === null) {
            createWindow();
        }
    });
}